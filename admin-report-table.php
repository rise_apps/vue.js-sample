<template id="admin-report-table">
    <table width="100%">
        <tr>
            <td><h1 style="transform:translateY(-13px)">{{reportingTitle}}</h1></td>
            <td>
                <div class="table-nav" v-if="paginate">
                    <div class="table-nav__left">
                        <div class="table-nav__limit">

                            <label for="table-limit">Items per page</label>
                            <div class="select select--alt">
                                <select id="table-limit" v-model="perPage">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                </select>
                                <span class="select__label">{{perPage}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="table-nav__right">
                        <div class="table-nav__item-count">{{totalItems}} items</div>
                        <button type="button" title="Previous Page" class="table-nav__page table-nav__page--prev" @click="previousPage" :disabled="page == 1"><span class="sr-only">Previous Page</span></button>
                        <button type="button" title="Next Page" class="table-nav__page table-nav__page--next" @click="nextPage" :disabled="page == totalPages"><span class="sr-only">Next Page</span></button>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    
    

    <div class="table-title" v-if="title">{{ title }}</div>
    <div class="table-wrapper">
        <table class="table table--m-1280 {{ className }}">
            <thead>
            <tr>
                <th v-for="(index, item) in items[0]">{{ index }}</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="item in items">
                <td v-for="(index, value) in item" data-heading="{{ index }}">{{ value }}</td>
            </tr>
            
            </tbody>
        </table>
    </div>
    <table>
        <tr>
            <div class="table-nav" style="margin-top:10px" v-if="paginate">
                <div class="table-nav__left">
                    <div class="table-nav__limit">
                        <label for="table-limit">Items per page</label>
                        <div class="select select--alt">
                            <select id="table-limit" v-model="perPage">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                            </select>
                            <span class="select__label">{{perPage}}</span>
                        </div>
                    </div>
                </div>
                <div class="table-nav__right">
                    <div class="table-nav__item-count">{{totalItems}} items</div>
                    <button type="button" title="Previous Page" class="table-nav__page table-nav__page--prev" @click="previousPage" :disabled="page == 1"><span class="sr-only">Previous Page</span></button>
                    <button type="button" title="Next Page" class="table-nav__page table-nav__page--next" @click="nextPage" :disabled="page == totalPages"><span class="sr-only">Next Page</span></button>
                </div>
            </div>
        </tr>
    </table>
    <div class="text-right">
        <a v-if="excel && items" v-on:click.stop="csvURL(excel)" class="export-btn">
            <img src="/public/images/download-icon.png" alt="">
            {{ exportButton }}
        </a>
    </div>
</template>
