import {get, POST, postForm} from "./global";

Vue.component('admin-report-table', {
    template: '#admin-report-table',
    props: {
        link: {
            type: String,
            required: true
        },
        excel: {
            type: String,
            required: true
        },
        className: {
            type: String,
            default: 'auto'
        },
        queries: Object,
        'end-date': {
            type: String,
            required: true
        },
        'start-date': String,
        paginate:  {
            type: Boolean,
            default: false
        },
    },
    data: function () {
        return {
            page: 1,
            perPage: 10,
            totalItems: 0,
            items: [],
            exportButton: 'Export',
            reportingTitle : this.$parent.reportingTitle,
            managers: function(that) { if ('managers' in that.$parent.filterElements && 'options' in that.$parent.filterElements.managers) { return that.$parent.filterElements.managers.options.length } return 0 }(this)

        }
    },
    computed: {
        totalPages: function() {
            return Math.ceil( this.totalItems / this.perPage );
        },
    },
    watch: {
        perPage: function() {
            this.page = 1;
            this.getReports();
        },
        page: 'getReports',

    },
    attached: function () {
        this.getReports();
    },
    methods: {
        getReports: function () {
            let self = this;
            self.$emit('request');
            POST(this.getUrl(), this.getData(), function(res) {
                self.$emit('response');

                res = JSON.parse(res);

                if (res.data.data.data && res.data.data.data.length) {
                    self.totalItems = res.data.data.total;
                    self.items = res.data.data.data;
                    self.title = res.data.title || '';
                } else if (res.data.data && res.data.data.length) {
                    self.totalItems = res.data.total;
                    self.items = res.data.data;
                } else {
                    self.items = null;
                }
            });
        },
        getData: function ()  {
            let theDate = moment().format('DD-MM-YYYY-h-mm-ss');
            let data = {
                _token: window.site.token,
                per_page: this.perPage,
                page: this.page,
                filename: this.reportingTitle.toLowerCase().replace(/\s/g,'')+theDate,

                from_date: encodeURIComponent(moment(this.startDate, 'DD/MM/YYYY').format('YYYY-MM-DD')),
                to_date: encodeURIComponent(moment(this.endDate, 'DD/MM/YYYY').format('YYYY-MM-DD')),
            };

            let self = this;

            Object.keys(this.queries).forEach(function(item) {
                data[item] = self.queries[item];
            });

            //Managers request shortener
            if(this.managers && ('managers' in data) && this.managers == data.managers.length) {
                data.managers = ['all'];
            }

            return data;
        },
        getUrl: function ()  {
            return this.link;
        },
        csvURL: function(type) {
            let self = this;
            self.exportButton = 'Exporting...';
            return POST(window.site.url + `/admin/report/export/${type}`, this.getData(), function(res) {
                self.exportButton = 'Export';
            }, 1);
        },
        nextPage: function() {
            if(this.page < this.totalPages) {
                this.page += 1;
            }
        },
        previousPage: function() {
            if(this.page > 1) {
                this.page -= 1;
            }
        },
    }
});
