<template id="calendar-template">
    <div class="calendar-container">
        <div class="calendar-date">
            <div>
                <h2>{{calendarTitle}}</h2>
                <button type="button" class="calendar-nav calendar-nav--prev" v-on:click="previousMonth">
                    <span class="sr-only">Previous month</span>
                </button>
                <button type="button" class="calendar-nav calendar-nav--next" v-on:click="nextMonth">
                    <span class="sr-only">Next month</span>
                </button>
            </div>
        </div>
        <div class="calendar">
            <div class="calendar-days">
                <div>Mon</div>
                <div>Tue</div>
                <div>Wed</div>
                <div>Thu</div>
                <div>Fri</div>
                <div>Sat</div>
                <div>Sun</div>
            </div>
            <div class="calendar-main">
                <div class="calendar-cell" v-bind:class="{'calendar-cell--has-more':(getCellProperties(n).length > 2)}"
                     v-for="n in 42" :data-day="getCellDate(n)" @click="cellClick">
                    <div class="calendar-dots">
                        <div v-for="m in getCellProperties(n).length"></div>
                    </div>
                    <ul class="calendar-list">
                        <li v-for="property in getCellProperties(n)">
                            <a href="#" @click.prevent="selectProperty(property)">
                                {{property.property_meta.name}}
                            </a>
                        </li>
                    </ul>
                    <button v-if="getCellProperties(n).length > 2" @click="showMore" type="button"
                            class="calendar-cell__more">Show {{ getCellProperties(n).length - 2 }} more
                    </button>
                </div>
            </div>
        </div>
    </div>
</template>
