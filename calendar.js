import {formatDate, get} from './global.js';

Vue.component( 'calendar', {

		template: '#calendar-template',

		data: function() {

			return {

				month: ( new Date() ).getMonth() + 1,
				year: ( new Date() ).getFullYear(),

				initialDataLoaded: false

			};

		},

		attached: function() {

			let vm = this.$parent;
			let that = this;



			get( this.getAPIUrl(), function( data ) {

				vm.properties = JSON.parse( data ).data.data;

				that.initialDataLoaded = true;

				console.log( JSON.parse( data ).data.data );

			} );

		},

		computed: {

			calendarTitle: function() {

				return moment( this.year + '-' + this.month, 'YYYY-M' ).format( 'MMMM YYYY' );

			},

			propertiesByDate: function() {

				var props = this.$parent.properties;

				props = _.groupBy( props, function( item ) {

					return moment( item.date ).format( 'YYYY-M-D' );

				} );

				return props;

			}

		},

		events: {

			updateProperties: function() {

				if( this.$parent.viewMode != 'calendar' ) {

					return;

				}

				if( !this.initialDataLoaded ) {

					return;

				}

				let vm = this.$parent;

				get( this.getAPIUrl(), function( data ) {

					vm.properties = JSON.parse( data ).data.data;

					console.log( JSON.parse( data ).data.data );

				} );

			}

		},

		methods: {

			getAPIUrl: function() {

				let vm = this.$parent;
				let tags = vm.filterTag.length ? encodeURIComponent( vm.filterTag.join( ',' ) ) : '';
				let regions = vm.filterLocation ? encodeURIComponent( vm.filterLocation.join( ',' ) ) : '';
				let perPage = 1000;
				let search = encodeURIComponent( vm.search );
				let month = encodeURIComponent( this.month );
				let year = encodeURIComponent( this.year );

				let url = window.site.url + '/api/v2/properties?per_page=' + perPage + '&category_ids=' + tags + '&region_ids=' + regions + '&qry=' + search + '&month=' + month + '&year=' + year;

				return url;

			},

			selectProperty: function( property ) {

				this.$parent.selectedProperty = property;

			},

			formatDate: formatDate,

			previousMonth: function() {

				if( this.month === 1 ) {

					this.month = 12;
					this.year = this.year - 1;

				} else {

					this.month = this.month - 1;

				}

				this.$emit( 'updateProperties' );

			},

			nextMonth: function() {

				if( this.month === 12 ) {

					this.month = 1;
					this.year = this.year + 1;

				} else {

					this.month = this.month + 1;

				}

				this.$emit( 'updateProperties' );

			},

			getCellDate: function( cellIndex ) {

				cellIndex += 1;

				var firstDay = moment( this.year + '-' + this.month + '-1', 'YYYY-M-D' ).format( 'e' );
				firstDay = parseInt( firstDay );
				firstDay = ( firstDay === 0 ) ? 7 : firstDay; // make Sunday = 7, not 0

				var daysInMonth = moment( this.year + '-' + this.month, 'YYYY-M' ).endOf( 'month' ).format( 'D' );
				daysInMonth = parseInt( daysInMonth );

				var result = ( cellIndex < firstDay ) ? false : cellIndex + 1 - firstDay;

				result = ( result > daysInMonth ) ? false : result;

				return result;

			},

			getCellProperties: function( cellIndex ) {

				var day = this.getCellDate( cellIndex );

				if( day ) {

					var props = this.propertiesByDate[this.year + '-' + this.month + '-' + day];

					if( props.length ) {

						return props;

					}

				}

				return [];

			},

			showMore: function( e ) {

				let cell = e.target.closest( '.calendar-cell' );

				cell.classList.toggle( 'calendar-cell--more' );

			},

			cellClick: function( e ) {

				let lists = document.querySelectorAll('.calendar-list--active');
				if (lists.length) {
					for (let i = 0; i < lists.length; i++) {
						lists[i].classList.remove('calendar-list--active');
					}
				}

				let list = e.target.closest('.calendar-cell').querySelector( '.calendar-list' );

				if( list ) {

					list.classList.add( 'calendar-list--active' );

				}

			}

		}

	} );
